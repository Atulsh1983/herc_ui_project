require('dotenv').config()
const path = require('path')
const Dotenv = require('dotenv-webpack');

module.exports = {
  webpack: config => {
    config.plugins = config.plugins || [];
    config.plugins = [
      new Dotenv({
        path: path.join(__dirname, '.env'),
        systemvars: true
      })
    ]
    return config
  }
}


const nextConfig = {
  serverRuntimeConfig: {
    live_api_url: 'server-runtime ' + process.env.API_URL
  },
  publicRuntimeConfig: {
    live_api_url: 'public-runtime ' + process.env.API_URL
  },
  env: {
    'MODE': process.env.MODE ? process.env.MODE : "dev"
  },
  distDir: 'build'
}
module.exports = nextConfig;