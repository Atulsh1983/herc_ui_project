import axios from "axios";
import apiUrl from "../constants/constants";
import fetch from 'isomorphic-unfetch';

export const guestAuthTokenCall = () => {
  let input = new FormData();
  input.append("grant_type", "client_credentials");
  input.append("portalId", 1);
  return axios({
    url: apiUrl.tokenGeneration,
    method: "post",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "",
      password: ""
    }
  });
};

export const refreshTokenCall = token => {
  let input = new FormData();
  input.append("refresh_token", token);
  input.append("grant_type", "refresh_token");
  return axios({
    url: apiUrl.tokenGeneration,
    method: "post",
    dataType: "jsonp",
    headers: {
      "content-type":
        "multipart/form-data; boundary=---011000010111000001101001"
    },
    data: input,
    auth: {
      username: "",
      password: ""
    }
  });
};

export async function fetchWithTimeout(timeoutMs, url, options) {
  let timeoutHandle = await fetch(url, options);
  return Promise.race([
    await timeoutHandle.json(),
    wait(timeoutMs)
  ])
}

function wait(ms) {
  return new Promise(function (resolve, reject) {
    setTimeout(resolve('timeout = ' + ms), ms);
  });
}
export async function fetchDataAsync(url, options) {
  console.log("=====async call req====", url, options.body)
  let timeoutHandle = await fetch(url, options);
  return await timeoutHandle.json();
}
