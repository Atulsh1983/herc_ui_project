import { all } from "redux-saga/effects";
import testSaga from "./rootSaga";
export default function* rootSaga() {
        yield all([
                testSaga()
        ]);
}