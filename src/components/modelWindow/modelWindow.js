import React, { Component } from 'react';
import styles from './modelWindow.module.scss';
import AuthControl from './auth/auth';
const ModelWindow = (props => {
	const { isModelWindowOpen, closeModelWindowHandler, isSignIn, isSignUp, isForget, signInHandler, signUpHandler, forgetHandler, submitHandler } = props;
	return (
		<>
			{isModelWindowOpen ? (
				<section className={styles.overLay}></section>) : ""}
			<article className={`${styles.modelWindow} ${isModelWindowOpen ? styles.ani : ""} ${isSignUp ? styles.modelHeight : ""}`}>
				<article className={styles.closeModelWindow}>
					<i className="fa fa-times" aria-hidden="true" onClick={closeModelWindowHandler}></i>
				</article>
				<AuthControl
					isSignIn={isSignIn}
					isSignUp={isSignUp}
					isForget={isForget}
					signInHandler={signInHandler}
					signUpHandler={signUpHandler}
					forgetHandler={forgetHandler}
					submitHandler={submitHandler} />
			</article>
		</>
	)
})

export default ModelWindow;

