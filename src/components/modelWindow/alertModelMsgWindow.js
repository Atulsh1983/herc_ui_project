import React, { Component } from 'react';
import styles from './alertModelMsgWindow.module.scss';

const AlertModelMsgWindow = (props => {
	const { closeAlertModelMsgWindowHandler, isAlertMsg } = props;
	return (
		<>
			{isAlertMsg ? (
				<section className={styles.overLay}></section>) : ""}
			<article className={`${styles.alertModelMsgWindow} ${isAlertMsg ? styles.ani : ""}`}>
				<article className={styles.msg}>
					<p>Submitted successfully.</p>
					<button onClick={closeAlertModelMsgWindowHandler}>Ok</button>
				</article>
			</article>
		</>
	)
})

export default AlertModelMsgWindow;

