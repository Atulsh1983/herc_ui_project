import React, { Component } from 'react';
import styles from './forget.module.scss';
const Forget = (props => {
	const { signInHandler, signUpHandler } = props
	return (
		<article className={styles.forgetWrapper}>
			<article className={styles.formWrapper}>
				<p>Recover your password <span>Fill in your e-mail address below and we will send an email with further instructions.</span></p>
				<article className={styles.formCtrlWrapper}>
					<form name="forget">
						<article className={styles.formCtrl}>
							<input type="text" name="email" placeholder="Email address" />
						</article>
						<article className={`${styles.formCtrl} ${styles.linkText}`}>
							<span onClick={signInHandler}>Already have an account?</span>
							<span onClick={signUpHandler}>Don't have an account?</span>
						</article>
						<article className={styles.formCtrl}>
							<button>Reset Password</button>
						</article>
					</form>
				</article>
			</article>
		</article>
	)
})

export default Forget;

