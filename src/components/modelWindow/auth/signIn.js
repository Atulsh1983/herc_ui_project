import React, { Component } from 'react';
import styles from './signIn.module.scss';
const SignIn = (props => {
	const { signUpHandler, forgetHandler, submitHandler } = props
	return (
		<article className={styles.signInWrapper}>
			<article className={styles.formWrapper}>
				<p>Loin to your account <span>Don't have an account? <i onClick={signUpHandler}>Sign Up Free!</i></span></p>
				<article className={styles.formCtrlWrapper}>
					<form name="signin">
						<article className={styles.formCtrl}>
							<input type="text" name="email" placeholder="Email address" />
						</article>
						<article className={styles.formCtrl}>
							<input type="password" name="password" placeholder="Password" />
							<i className="fa fa-eye" aria-hidden="true"></i>
						</article>
						<article className={`${styles.formCtrl} ${styles.linkText}`}>
							<label>
								<input type="checkbox" /> Remember me
							</label>
							<span onClick={forgetHandler}>Forgot password?</span>
						</article>
						<article className={styles.formCtrl}>
							<button type="button" onClick={() => submitHandler("signIn")}>Log In</button>
						</article>
					</form>
				</article>
			</article>
		</article>
	)
})

export default SignIn;

