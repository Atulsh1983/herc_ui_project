import React, { Component } from 'react';
import styles from './signUp.module.scss';
import Router from 'next/router';
const SignUp = (props => {
	const { signInHandler, submitHandler } = props
	return (
		<article className={styles.signUpWrapper}>
			<article className={styles.formWrapper}>
				<p>Sign up for free!</p>
				<article className={styles.formCtrlWrapper}>
					<form name="signup">
						<article className={styles.formCtrl}>
							<input type="text" name="firstname" placeholder="First name" />
						</article>
						<article className={styles.formCtrl}>
							<input type="text" name="lastname" placeholder="Last name" />
						</article>
						<article className={styles.formCtrl}>
							<input type="text" name="email" placeholder="Email address" />
						</article>
						<article className={styles.formCtrl}>
							<input type="password" name="password" placeholder="Password" />
							<i className="fa fa-eye" aria-hidden="true"></i>
						</article>
						<article className={`${styles.formCtrl} ${styles.selectOpt}`}>
							<span>Select an option</span>
							<label>
								<input type="radio" name="option" /> Mentee
							</label>
							<label>
								<input type="radio" name="option" /> Advisor
							</label>
							<label>
								<input type="radio" name="option" /> Other
							</label>
						</article>
						<article className={`${styles.formCtrl} ${styles.linkText}`}>
							<span onClick={signInHandler}>Already a member? LoginIn</span>
						</article>
						<article className={styles.formCtrl}>
							<p className={styles.text}>By click Agree & Join, you agree to the Herconnections User Agreement, Privacy Policy, and Cookie Policy</p>
						</article>
						<article className={styles.formCtrl}>
							<button type="button" onClick={() => submitHandler("signUp")}> Agree and Join</button>
						</article>
					</form>
				</article>
			</article>
		</article>
	)
})

export default SignUp;

