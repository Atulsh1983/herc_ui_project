import React, { Component } from 'react';
import styles from './auth.module.scss';
import SignIn from './signIn';
import SignUp from './signUp';
import ForgetPassword from './forget';
const Auth = (props => {
	const { isSignIn, isSignUp, isForget, signInHandler, signUpHandler, forgetHandler, submitHandler } = props
	return (
		<article className={styles.authWrapper}>
			<article className={styles.authBox}>
				<p>Loin using social media to get quick access</p>
				<article className={styles.socialBtnWrapper}>
					<button className={styles.facebook}><i className="fa fa-facebook" aria-hidden="true"></i> Signin with facebook</button>
					<button className={styles.google}><i className="fa fa-google" aria-hidden="true"></i> Signin with google</button>
					<button className={styles.linkedin}><i className="fa fa-linkedin" aria-hidden="true"></i> Signin with linkedin</button>
				</article>
			</article>
			<article className={styles.authBox}>
				{isSignIn && <SignIn signUpHandler={signUpHandler} forgetHandler={forgetHandler} submitHandler={submitHandler} />}
				{isSignUp && <SignUp signInHandler={signInHandler} submitHandler={submitHandler} />}
				{isForget && <ForgetPassword signInHandler={signInHandler} signUpHandler={signUpHandler} />}
			</article>
		</article>
	)
})

export default Auth;

