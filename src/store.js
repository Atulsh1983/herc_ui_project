import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware, { END } from "redux-saga";
// import createMemoryHistory from "history/createMemoryHistory";
import rootSaga from "./sagas/combinedSaga";
import combinedReducer from './reducers/combinedReducers';

function configureStore(preloadedState) {
    // Make exception for redux dev tools
    /* eslint-disable no-underscore-dangle */
    /* eslint-disable no-undef */
    const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;
    /* eslint-enable */
    const sagaMiddleware = createSagaMiddleware();

    const store = createStore(
        combinedReducer,
        preloadedState,
        composeEnhancers(applyMiddleware(sagaMiddleware))
    );
    store.sagaTask = sagaMiddleware.run(rootSaga);

    return store;
}

export default configureStore;
