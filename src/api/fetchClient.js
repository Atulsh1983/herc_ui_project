import axios from "axios";
import apiUrl from "../constants/constants";
import { messages } from "../constants/constants";

var jwtDecode = require("jwt-decode");
var renewTokenPromise;
const fetchClient = () => {
  const defaultOptions = {
    baseURL: "",
    method: "GET",
    //timeout: 2000,
    headers: {
      "Content-Type": "application/json",
      "Accept": "application/json"
    },
  };

  // Create instance
  let instance = axios.create(defaultOptions);

  /************ Start User token refresh **************** */
  var isTokenRefreshing = false;
  // Set the AUTH token for any request
  instance.interceptors.request.use(function (config) {
    config.headers.Authorization = "Bearer " + utils.getAuthToken(); //send token in every call............
    //    console.log(config.headers.Authorization, "config.headers.Authorization")
    if (
      typeof window !== "undefined" &&
      parseInt(localStorage.getItem("isAuth")) &&
      !isTokenRefreshing
    ) {
      //Refresh token only if user is logged in
      const accessTokenDecoded = jwtDecode(utils.getAuthToken());
      const refreshTokenDecoded = jwtDecode(utils.getRefreshToken());

      //rfresh token only in case user is logged in and no request for refresh is running
      const isRefreshTokenExpired = refreshTokenDecoded.exp
        ? refreshTokenDecoded.exp * 1000 <= new Date().getTime()
        : true;
      const isAccessTokenExpired = accessTokenDecoded.exp
        ? accessTokenDecoded.exp * 1000 <= new Date().getTime()
        : true;
      const tokenExpireDelta =
        new Date().getTime() -
        (parseInt(localStorage.getItem("tokenGenerationTime")) +
          parseInt(localStorage.getItem("tokenExpiry")) * 1000); //make it ms and find delta
      if (isRefreshTokenExpired) {
        //If refresh token is expired then logout and go to Home Page
        localStorage.clear();
        window.location.href = "/user/login?sessionExpired=refresh";
        return;
      }

      if (tokenExpireDelta > -600000 || isAccessTokenExpired) {
        //refersh token before 10 seconds of expiration
        isTokenRefreshing = true;

        // renewToken performs authentication using username/password saved in sessionStorage/localStorage
        renewTokenPromise = new Promise((resolve, reject) => {
          utils.refreshToken(apiUrl.tokenGeneration).then(response => {
            config.headers.Authorization =
              "Bearer " + response.data.access_token;
            // Get your config from the response
            utils.storeAuthDetails(response.data);

            isTokenRefreshing = false;
            // Resolve the promise
            resolve(config);
          }, reject);

          // Or when you don't need an HTTP request just resolve
          // resolve(config);
        });
        return renewTokenPromise;
      } //end if
    } else {
      if (isTokenRefreshing) {
        return renewTokenPromise.then(function () {
          config.headers.Authorization = "Bearer " + utils.getAuthToken();
          return config;
        });
      }
      //If user is not logged in.......
      const accessTokenDecoded = jwtDecode(utils.getAuthToken());
      const isAccessTokenExpired = accessTokenDecoded.exp
        ? accessTokenDecoded.exp * 1000 <= new Date().getTime()
        : true;

      if (isAccessTokenExpired && typeof window !== "undefined") {
        console.warn("Guest token expired");
        localStorage.clear();
        window.location.href = "/?sessionExpired=true";
        return;
      }
    }

    return config;
  });
  /***** End User token refresh ********** */

  /***********************************Start Response Interceptor for token handling*************************************** */
  instance.interceptors.response.use(
    response => response,
    error => {
      //console.log(error, "responseError");
      let value = error.response;
      if (error.code && error.code == 'ETIMEDOUT') {
        error.errorMessage = messages.generic.connection_timeout;
        console.log("responseError", error.errorMessage, error.config.url);
        return Promise.reject(error);
      }
      //To handle network error thrown by axios(can be server OR CLIENT)
      if (!error.response.status) {
        error.errorMessage = messages.generic.networkError;
        console.log("responseError", error.errorMessage, error.config.url);
        return Promise.reject(error);
      }

      const httpCode = error.response.status;
      const errMessage = messages.generic.httpErrors[httpCode];

      error.errorMessage = errMessage;
      if (value.status === 401) {
        //utils.getRefreshToken();
        console.log("responseError", error.errorMessage, error.config.url);
        return instance(error.config);
      }
      if (error.errno && error.errno) {
        error.errorMessage = messages.generic.error;
        console.log("responseError", error.errorMessage, error.config.url);
        return Promise.reject(error);
      }
      return Promise.reject(error);
    }
  );
  /************************************End Response Interceptor for token handling*************************************** */

  return instance;
};

export default fetchClient();
