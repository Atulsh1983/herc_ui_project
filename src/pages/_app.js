import React from 'react'
import App from "next/app";
import Layout from "./../layouts/Layout";
import Router from 'next/router';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import withRedux from "next-redux-wrapper";
import { Provider } from "react-redux";
import withReduxSaga from "next-redux-saga";
import configureStore from "../store";
import Head from 'next/head'
import "../style.css";
class MyApp extends App {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        Router.events.on('routeChangeStart', () => NProgress.start());
        Router.events.on('routeChangeComplete', () => NProgress.done());
        Router.events.on('routeChangeError', () => NProgress.done());
    }

    render() {
        const { Component, pageProps, store, router } = this.props;
        const { history } = this.state;

        return (
            <>
                <Head>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
                </Head>
                <Provider store={store}>
                    <Layout>
                        <Component {...pageProps} {...router} history={history} />
                    </Layout>
                </Provider>
            </>
        );
    }
}

export default withRedux(configureStore)(withReduxSaga(MyApp));