import React, { Component } from "react";
import HomePage from "../modules/home";
class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    typeof window !== 'undefined' && localStorage.setItem('componentName', 'home')
    return (
      <main>
        <HomePage />
      </main>
    );
  }
}

export default Index;