import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';
class CommonFooter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			initScroll: true
		}
		//this.scrollHandler = this.scrollHandler.bind(this);
	}

	/* scrollHandler() {
		console.log('hi')
		if (typeof window !== "undefined") {
			window.scrollTo({
				top: 0, // could be negative value
				left: 0,
				behavior: "smooth"
			});
		}
	} */
	render() {
		const { styles } = this.props;
		const { initScroll } = this.state;
		return (
			<>
				<Container fluid="true" className={styles.commonFooter}>
					<Row className={styles.footerTop}>
						<Container>
							<Row>
								<article className={styles.footerAbout}>
									<article className={styles.logo}>
										<a href="#"><img src="http://www.customerengagepro.com/wp-content/uploads/2020/07/52b566_06d632d2fb5e41a2b68b6b2ffdbcc9b3_mv2.png" alt="Logo" /></a>
									</article>
									<p>HerConnections provides its members access to personalized career advice from anywhere globally on any device at any time from successful women in the STEM field.</p>
									<ul>
										<li><a href="#"><i className="fa fa-facebook-f"></i></a></li>
										<li><a href="#"><i className="fa fa-twitter"></i></a></li>
										<li><a href="#"><i className="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i className="fa fa-instagram"></i></a></li>
									</ul>
								</article>

								<article className={styles.footerLink}>
									<article className={styles.linkCol}>
										<article className={styles.footerTitle}>
											<h6>Sitemap</h6>
										</article>
										<article className={styles.linkWrapper}>
											<ul>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>Why join us?</a>
												</li>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>Advice</a>
												</li>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>About us</a>
												</li>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>Contact us</a>
												</li>
											</ul>
										</article>
									</article>
									<article className={styles.linkCol}>
										<article className={styles.footerTitle}>
											<h6>Support</h6>
										</article>
										<article className={styles.linkWrapper}>
											<ul>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>FAQS</a>
												</li>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>Privacy Polict</a>
												</li>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>Terms & Conditions</a>
												</li>
												<li>
													<a href="#"><i className="fa fa-angle-right"></i>Disclaimer</a>
												</li>
											</ul>
										</article>
									</article>
									<article className={styles.linkCol}>
										<article className={styles.footerTitle}>
											<h6>Contact Us</h6>
										</article>
										<article className={styles.footerAddress}>
											<ul>
												<li>
													<article className={styles.icon}>
														<i className="fa fa-home"></i>
													</article>
													<article className={styles.box}>
														<p>Noida, Uttar Pradesh</p>
													</article>
												</li>
												<li>
													<article className={styles.icon}>
														<i className="fa fa-phone"></i>
													</article>
													<article className={styles.box}>
														<p>+91-XXX-XXXX-XXX</p>
													</article>
												</li>
												<li>
													<article className={styles.icon}>
														<i className="fa fa-envelope-o"></i>
													</article>
													<article className={styles.box}>
														<p>info@domainname.com</p>
													</article>
												</li>
											</ul>
										</article>
									</article>
								</article>
							</Row>
						</Container>
					</Row>
					<Row className={styles.footerCopyright}>
						<Container>
							<span>All rights reserved © HerConnections</span>
						</Container>
					</Row>
				</Container>
				{initScroll == true ? (
					<span
						className={`${styles.backToTop} ${styles.ani}
                        : styles.backToTop`}
					//onClick={this.scrollHandler}
					><i className="fa fa-angle-up"></i></span>
				) : ("")}
			</>
		)
	}
}

export default CommonFooter;

