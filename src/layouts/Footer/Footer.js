import React, { Component } from 'react';
import CommonFooter from './commonFooter';
import MyProfileFooter from './myprofileFooter';
import styles from './footer.module.scss';
class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    let componentName = typeof window !== 'undefined' && localStorage.getItem("componentName");
    console.log(componentName, '[componentName - footer]')
    return (
      <>
        {componentName === "signIn" && <MyProfileFooter styles={styles} />}
        {componentName === "signUp" && <MyProfileFooter styles={styles} />}
        {componentName === "myprofile" && <MyProfileFooter styles={styles} />}
        {componentName === "mystatus" && <MyProfileFooter styles={styles} />}
        {componentName === "myaccount" && <MyProfileFooter styles={styles} />}
        {componentName === "notifications" && <MyProfileFooter styles={styles} />}
        {componentName === "home" && <CommonFooter styles={styles} />}
      </>
    )
  }
}

export default Footer;

