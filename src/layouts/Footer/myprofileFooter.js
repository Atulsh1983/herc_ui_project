import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap';
class CommonFooter extends Component {
	constructor(props) {
		super(props);
		this.state = {
			initScroll: true
		}
		//this.scrollHandler = this.scrollHandler.bind(this);
	}
	/* scrollHandler() {
		if (typeof window !== "undefined") {
			window.scrollTo({
				top: 0, // could be negative value
				left: 0,
				behavior: "smooth"
			});
		}
	} */

	render() {
		const { styles } = this.props;
		const { initScroll } = this.state;
		return (
			<>
				<Container fluid="true" className={styles.myprofileFooter}>
					<Row className={styles.footerCopyright}>
						<Container>
							<span>All rights reserved © HerConnections</span>
						</Container>
					</Row>
				</Container>
				{initScroll == true ? (
					<span
						className={`${styles.backToTop} ${styles.ani}
                        : styles.backToTop`}
					//onClick={this.scrollHandler}
					><i className="fa fa-angle-up"></i></span>
				) : ("")}
			</>
		)
	}
}

export default CommonFooter;

