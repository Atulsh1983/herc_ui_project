import React, { Component } from 'react'
import Header from './Header/Header'
import Footer from './Footer/Footer';
class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    return (
      <>
        <Header />
        <section>{this.props.children}</section>
        <Footer />
      </>
    )
  }

}

export default Layout;