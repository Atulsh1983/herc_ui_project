import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import Router from 'next/router';
import ModelWindow from './../../components/modelWindow/modelWindow';
import AlertModelMsgWindow from './../../components/modelWindow/alertModelMsgWindow';

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isModelWindowOpen: false,
			isSignIn: true,
			isSignUp: false,
			isForget: false,
			isAlertMsg: false
		}
		this.modelWindowHandler = this.modelWindowHandler.bind(this);
		this.closeModelWindowHandler = this.closeModelWindowHandler.bind(this);
		this.signInHandler = this.signInHandler.bind(this);
		this.signUpHandler = this.signUpHandler.bind(this);
		this.forgetHandler = this.forgetHandler.bind(this);
		this.submitHandler = this.submitHandler.bind(this);
		this.closeAlertModelMsgWindowHandler = this.closeAlertModelMsgWindowHandler.bind(this);
	}
	modelWindowHandler() {
		this.setState({
			isModelWindowOpen: true
		})
	}
	closeModelWindowHandler() {
		this.setState({
			isModelWindowOpen: false
		})
	}
	closeAlertModelMsgWindowHandler() {
		this.setState({
			isAlertMsg: false
		})
		Router.push('/myaccount', '/myaccount', { shallow: true });
	}
	signInHandler() {
		this.setState({
			isSignIn: true,
			isSignUp: false,
			isForget: false
		})
	}
	signUpHandler() {
		this.setState({
			isSignIn: false,
			isSignUp: true,
			isForget: false,
		})
	}
	forgetHandler() {
		this.setState({
			isSignIn: false,
			isSignUp: false,
			isForget: true
		})
	}

	submitHandler(componentName) {
		switch (componentName) {
			case "signIn":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				Router.push('/mystatus', '/mystatus', { shallow: true });
				break;
			case "signUp":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				this.setState({
					isAlertMsg: true,
					isModelWindowOpen: false,
				})
				break;
		}
	}
	render() {
		const { styles } = this.props;
		const { isModelWindowOpen, isSignIn, isSignUp, isForget, isAlertMsg } = this.state;
		return (
			<>
				<Container fluid={true} className={styles.commonHeader}>
					<Container fluid={true} className={styles.headerTop}>
						<Container>
							<Row>
								<article className={styles.headerContact}>
									<ul>
										<li><span>Noida, Uttar Pradesh</span></li>
										<li><span>info@domainname.com</span></li>
									</ul>
								</article>
							</Row>
						</Container>
					</Container>
					<Container fluid={true} className={styles.headerLogoSupport}>
						<Container>
							<Row className={styles.spaceBetween}>
								<article className={styles.logo}>
									<img src="http://www.customerengagepro.com/wp-content/uploads/2020/07/52b566_06d632d2fb5e41a2b68b6b2ffdbcc9b3_mv2.png" alt="Logo" />
								</article>
								<article className={styles.supportButton}>
									<article className={styles.support}>
										<article className={styles.cont}>
											<p>Need Help? call us free</p>
											<span>+91-XXX-XXXX-XXX</span>
										</article>
									</article>
									<article className={styles.button}>
										<button className={styles.authBtn} onClick={this.modelWindowHandler}>Log In / Sign Up</button>
									</article>
								</article>
							</Row>
						</Container>
					</Container>
					<Container fluid={true} className={styles.navigation}>
						<Container>
							<Row>
								<nav>
									<ul>
										<li><a href="#">Why Join Us?</a></li>
										<li><a href="#">Advice</a></li>
										<li><a href="#">More <i className="fa fa-angle-down"></i></a></li>
										<li><a href="#">About Us</a></li>
										<li><a href="#">Contact Us</a></li>
									</ul>
								</nav>
							</Row>
						</Container>
					</Container>
				</Container>
				<ModelWindow
					isModelWindowOpen={isModelWindowOpen}
					closeModelWindowHandler={this.closeModelWindowHandler}
					isSignIn={isSignIn}
					isSignUp={isSignUp}
					isForget={isForget}
					signInHandler={this.signInHandler}
					signUpHandler={this.signUpHandler}
					forgetHandler={this.forgetHandler}
					submitHandler={this.submitHandler}
					isAlertMsg={isAlertMsg} />

				{isAlertMsg && (<AlertModelMsgWindow closeAlertModelMsgWindowHandler={this.closeAlertModelMsgWindowHandler} isAlertMsg={isAlertMsg} />)}
			</>
		)
	}
}

export default Header;