import css from 'styled-jsx/css';
export default css.global`
@charset "UTF-8";
.myprofileHeader {
	position: relative;
	top: 0;
	z-index: 1;
	background: #fff;
	display: flex;
	flex-direction: column;
	padding-right: 0;
	padding-left: 0;
  }
  .myprofileHeader .headerWrapper {
	background-color: #07294d;
	padding-top: 10px;
	padding-bottom: 10px;
	display: flex;
	align-items: center;
	width: 100%;
	height: 65px;
  }
  .myprofileHeader .headerWrapper .spaceBetween {
	display: flex;
	align-items: center;
	justify-content: space-between;
  }
  .myprofileHeader .headerWrapper .spaceBetween .logo {
	margin: 0;
	padding: 0;
  }
  .myprofileHeader .headerWrapper .spaceBetween .logo img {
	width: 160px;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact {
	margin: 0;
	padding: 0;
	display: flex;
	width: auto;
	justify-content: flex-end;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul {
	margin: 0;
	padding: 0;
	list-style: none;
	display: flex;
	flex-direction: row;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul li {
	margin: 0;
	padding: 0;
	display: flex;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul li span {
	color: #dee2e6;
	font-size: 15px;
	font-weight: 400;
	margin-left: 10px;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul li:first-child {
	margin-right: 45px;
	position: relative;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul li:first-child:before {
	content: "";
	position: absolute;
	top: 2px;
	left: -25px;
	width: 27px;
	height: 18px;
	background: url("./../../../public/images/spriteImg.png") no-repeat;
	background-position: -1px -51px;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul li:last-child {
	position: relative;
  }
  .myprofileHeader .headerWrapper .spaceBetween .headerContact ul li:last-child:before {
	content: "";
	position: absolute;
	top: 2px;
	left: -25px;
	width: 27px;
	height: 18px;
	background: url("./../../../public/images/spriteImg.png") no-repeat;
	background-position: -6px 0;
  }
  .myprofileHeader .navigation {
	margin: 0;
	padding: 0;
	display: flex;
	flex-direction: row;
	justify-content: right;
	align-items: center;
	width: 100%;
  }
  .myprofileHeader .navigation nav {
	margin: 0;
	padding: 0;
	display: flex;
	flex-direction: row;
	width: 100%;
	justify-content: flex-end;
  }
  .myprofileHeader .navigation nav ul {
	margin: 0;
	padding: 0;
	list-style: none;
	display: flex;
	flex-direction: row;
  }
  .myprofileHeader .navigation nav ul li {
	margin: 0 40px 0 0;
	padding: 0;
	display: flex;
  }
  .myprofileHeader .navigation nav ul li a {
	font-size: 14px;
	font-family: montserrat,sans-serif;
	font-weight: 700;
	color: #07294d;
	text-transform: uppercase;
	transition: all .4s linear;
	padding: 25px 0;
	text-decoration: none;
  }
  .myprofileHeader .navigation nav ul li a:hover {
	color: #ffc600;
  }
  .myprofileHeader .navigation nav ul li:last-child {
	margin-right: 0;
  }
  .myprofileHeader .navigation nav ul li.active a {
	color: #ffc600;
  } 

`