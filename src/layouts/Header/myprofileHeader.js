import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import Router from 'next/router';
class MyProfileHeader extends Component {
	constructor(props) {
		super(props);
		this.state = {}
		this.loggoutHandler = this.loggoutHandler.bind(this);
		this.componentChangeHandler = this.componentChangeHandler.bind(this);
	}
	loggoutHandler(componentName) {
		typeof window !== 'undefined' && localStorage.setItem('componentName', componentName);
		Router.push('/', '/', { shallow: true });
	}
	componentChangeHandler(componentName) {
		switch (componentName) {
			case "home":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				Router.push('/', '/', { shallow: true });
				break;
			case "myprofile":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				Router.push('/myprofile', '/myprofile', { shallow: true });
				break;
			case "mystatus":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				Router.push('/mystatus', '/mystatus', { shallow: true });
				break;
			case "notifications":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				Router.push('/notifications', '/notifications', { shallow: true });
				break;
		}
	}
	render() {
		const { styles } = this.props;
		const { } = this.state;
		return (
			<Container fluid={true} className={styles.myprofileHeader}>
				<Container fluid={true} className={styles.headerWrapper}>
					<Container>
						<Row className={styles.spaceBetween}>
							<article className={styles.logo}>
								<img src="http://www.customerengagepro.com/wp-content/uploads/2020/07/52b566_06d632d2fb5e41a2b68b6b2ffdbcc9b3_mv2.png" alt="Logo" onClick={() => this.componentChangeHandler("home")} />
							</article>
							<article className={styles.navWrapper}>
								<nav>
									<ul>
										<li className={styles.homeIcon}>
											<span className={styles.home} onClick={() => this.componentChangeHandler("home")}>	</span>
										</li>
										<li className={styles.notificationCell}>
											<span className={styles.notification}>
												<i>2</i>
											</span>
											<article className={styles.dropMenu}>
												<ul>
													<li>
														<article className={`${styles.logo} ${styles.nlogo1}`}></article>
														<article className={styles.content}><span>Your job alert</span>
															<p>Sanjay Kr., <i>19</i> new jobs for <i>Front-end Developer</i> in <i>Noida Area</i></p></article>

													</li>
													<li>
														<article className={`${styles.logo} ${styles.nlogo3}`}></article>
														<article className={styles.content}><p>Since you passed the Cascading Style Sheets (CSS) skill assessment, we think you may be a good fit for the</p></article>
													</li>
												</ul>
												<span className={styles.moreNotification}>
													<i onClick={() => this.componentChangeHandler("notifications")}>More Notifications</i>
												</span>
											</article>
										</li>
										<li className={styles.userNav}>
											<i className={styles.faUser}></i>
											<article className={styles.dropMenu}>
												<ul>
													<li>
														<span onClick={() => this.componentChangeHandler("myprofile")}>My Profile</span>
													</li>
													<li>
														<span onClick={() => this.componentChangeHandler("mystatus")}>My Home</span>
													</li>
													<li>
														<button onClick={() => this.loggoutHandler("home")}>Logout</button>
													</li>
												</ul>
											</article>
										</li>
									</ul>
								</nav>
							</article>
						</Row>
					</Container>
				</Container>
			</Container>
		)
	}
}

export default MyProfileHeader;