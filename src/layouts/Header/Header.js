import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import CommonHeader from './commonHeader';
import MyProfileHeader from './myprofileHeader';
import styles from './header.module.scss';
class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    let componentName = typeof window !== 'undefined' && localStorage.getItem("componentName");
    console.log(componentName, '[componentName - header]')
    return (
      <>
        {componentName === "signIn" && <MyProfileHeader styles={styles} />}
        {componentName === "signUp" && <MyProfileHeader styles={styles} />}
        {componentName === "myprofile" && <MyProfileHeader styles={styles} />}
        {componentName === "mystatus" && <MyProfileHeader styles={styles} />}
        {componentName === "myaccount" && <MyProfileHeader styles={styles} />}
        {componentName === "notifications" && <MyProfileHeader styles={styles} />}
        {componentName === "home" && <CommonHeader styles={styles} />}
      </>
    )
  }
}

export default Header;