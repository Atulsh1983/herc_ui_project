import { combineReducers } from 'redux';
import rootReducer from "./rootReducer";
export const combinedReducer = combineReducers({
    rootReducer
});

export default combinedReducer;