//import gblFunc from "../globals/globalFunctions";
import moment from "moment";
export const prodEnv =
    typeof window !== "undefined"
        ? window.location.hostname == ""
            ? 1 //live
            : 0 //qa or staging
        : 1;
let endPointUrl = ""; // default set if .env file not set on your local project.
if (process.env.MODE == "staging") {
    endPointUrl = "";
} else if (process.env.MODE == "dev" || process.env.MODE == "development") {
    endPointUrl = "";
} else if (process.env.MODE == "custom" && process.env.CUSTOM_URL) {
    endPointUrl = process.env.CUSTOM_URL;
}
export const imgBaseUrl = "";
export const imgBaseUrlDev = "";
export const access_token = "";
export const getApi = function (apiKey, dynamicVals) {
    //method to replace dynamic values in the api url@params - (apiKey in the constants, array of dynamic values)

    var FormattedUrl = apiUrl[apiKey]["url"];
    for (var i = 0; i < dynamicVals.length; i++) {
        FormattedUrl = FormattedUrl.replace(
            apiUrl[apiKey]["dynamicValues"][i],
            dynamicVals[i]
        );
    }
    return FormattedUrl;
};

export const baseUrls = {};

const apiUrl = {};
export default apiUrl;

export const PRODUCTION_URL = "";
export const STAGING_URL = "";

export const apiCallTypes = {
    GET: "GET",
    POST: "POST",
    PUT: "PUT",
    DELETE: "DELETE"
};


export const camelCase = obj => {
    let rtn = obj;
    if (typeof obj === "object") {
        if (obj instanceof Array) {
            rtn = obj.map(camelCase);
        } else {
            for (let key in obj) {
                if (obj.hasOwnProperty(key)) {
                    const newKey = key.replace(/(_\w)/g, k => k[1].toUpperCase());
                    rtn[newKey] = camelCase(obj[key]);

                    if (newKey != key) {
                        delete obj[key];
                    }
                }
            }
        }
    }
    return rtn;
};

export const snakeCase = obj => {
    //return false;
    if (typeof obj != "object") return obj;

    for (var oldName in obj) {
        // Camel to underscore
        let newName = oldName.replace(/([A-Z])/g, function ($1) {
            return "_" + $1.toLowerCase();
        });
        // Only process if names are different
        if (newName != oldName) {
            // Check for the old property name to avoid a ReferenceError in strict mode.
            if (obj.hasOwnProperty(oldName)) {
                obj[newName] = obj[oldName];
                delete obj[oldName];
            }
        }
        // Recursion
        if (typeof obj[newName] == "object") {
            obj[newName] = snakeCase(obj[newName]);
        }
    }
    return obj;
};

export const imageFileExtension = [
    ".jpeg",
    ".jpg",
    ".png",
    ".bmp",
    ".tif",
    ".JPEG",
    ".JPG",
    ".PNG",
    ".BMP",
    ".TIF"
];


export const getYearOrMonth = (type, value) => {
    if (type == "year") {
        let difference = value[1] - value[0];
        let year = [];
        while (difference >= 0) {
            year.push(value[0]);
            value[0] += 1;
            difference--;
        }
        return year.reverse();
    } else {
        return ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"];
    }
};

export const setCookie = (cname, cvalue, hours) => {
    let d = new Date();
    d.setTime(d.getTime() + hours * 60 * 60 * 1000);
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

export const getCookie = cname => {
    let name = cname + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == " ") {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
};

export const messages = {
    login: {
        success: "You have successfully logged in!",
        error: "",
        confirmation: "",
        acknowledgement: ""
    },
    otp: {
        success: "OTP has been sent to your registered mobile number.",
        verified: "Your mobile number has been verified successfully.",
        verifyError: "Incorrect OTP entered.",
        alreadyVerifiedMobile:
            "This mobile number is verified with us already. Please use different one."
    },

    register: {
        success:
            "Your registration is successful. Please check your email for verification.",
        error: {},
        confirmation: "",
        acknowledgement: ""
    },

    changePassword: {
        success: "Your password changed successfully",
        failed: "Your old password is incorrect",
        confirmation: "",
        acknowledgement: ""
    },
    generic: {
        success: "Thank you for contacting us.",
        error: "Something went wrong. Please try again later.",
        confirmation: "",
        acknowledgement: "",
        generic: "",
        request_timeout: "Request timeout",
        connection_timeout: "Connection timeout",
        httpErrors: {
            400: "Oops! Something went wrong. We are working on fixing the problem.",
            500: "Oops! Something went wrong. We are working on fixing the problem.",
            404: "Oops! Something went wrong. We are working on fixing the problem.",
            403: "You don't have permission to access this resource",
            409: "Can not save same record",
            401: "You are not authorized",
            503: "Service unavailable",
            801: "No data found",
            803: "Incorrect password",
            702: "Record already exists",
            701: "Missing validation"
        },
        networkError:
            "Oops! Something went wrong. Please check your network connection and try again."
    },
    forgotPssword: {
        success: "Password verification email has been sent to you",
        error: "Oops! We couldn't locate this email. Please register.",
        confirmation: "",
        acknowledgement: ""
    },
    resetPassword: {
        success: "Your password has been reset successfully",
        error: "",
        confirmation: "",
        acknowledgement: ""
    },
    resetPasswordTokenExpire: {
        success:
            "*Your link to reset password has expired. Please re-apply from ‘Forgot Password’ link to try again!",
        error: "",
        confirmation: "",
        acknowledgement: ""
    }
};


