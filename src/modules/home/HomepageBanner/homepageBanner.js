import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import styles from "./homepageBanner.module.scss";
export default function HomePage(props) {
	return (
		<Container fluid={true} className={styles.banner}>
			<Container>
				<h6>HerConnections provides its members access to <i>personalized career advice</i> from <i>anywhere</i> globally on <i>any device</i> at <i>any time</i> from successful women in the STEM field.</h6>
			</Container>
		</Container>
	)
}