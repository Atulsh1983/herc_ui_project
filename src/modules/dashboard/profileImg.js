import React from 'react';
import styles from "./profileImg.module.scss";
const ProfileImg = props => {
	return (
		<section className={styles.profileContentPos}>
			<section className={styles.profileContent}>
				<article className={styles.profileContentWrapper}>
					<form name="profileImg" autoCapitalize="off">
						<article className={styles.imgWrapper}>
							<article className={styles.fileuploadBg}>
								<label htmlFor="docFile">Upload Profile Picture
							<input type="file" onChange={event => props.imagefileChangedHandler(event)} name="file" id="docFile" accept="image/*" />
								</label>
							</article>
							{/* <img src={""} alt="" /> */}
						</article>
					</form>
					<article className={styles.attentionWrapper}>
						<h1>Sanjay Kr. Singh</h1>
						<button>Make Profile Public</button>
					</article>
				</article>
			</section>
		</section>
	)
}
export default ProfileImg;