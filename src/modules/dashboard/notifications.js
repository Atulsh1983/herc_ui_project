import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import styles from "./notifications.module.scss";
export default function Notifications(props) {
	return (
		<>
			<article className={styles.titleWrapper}>
				<h2>My Notifications</h2>
				<span className={styles.subTitle}>
					You have (2) new notifications. Improve your notifications
[<i>View settings</i>]
				</span>
			</article>
			<Row className={styles.notifications}>
				<ul>
					<li className={styles.newNotification}>
						<article className={`${styles.logo} ${styles.nlogo1}`}></article>
						<article className={styles.content}><span>Your job alert</span>
							<p>Sanjay Kr., <i>19</i> new jobs for <i>Front-end Developer</i> in <i>Noida Area</i></p></article>
						<article className={styles.settingsDays}>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" data-supported-dps="24x24" fill="currentColor" width="24" height="24" focusable="false">
								<path d="M2 10h4v4H2v-4zm8 4h4v-4h-4v4zm8-4v4h4v-4h-4z"></path>
							</svg>
							<i>4h</i>
						</article>
					</li>
					<li className={styles.newNotification}>
						<article className={`${styles.logo} ${styles.nlogo3}`}></article>
						<article className={styles.content}><p>Since you passed the Cascading Style Sheets (CSS) skill assessment, we think you may be a good fit for the</p></article>
						<article className={styles.settingsDays}>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" data-supported-dps="24x24" fill="currentColor" width="24" height="24" focusable="false">
								<path d="M2 10h4v4H2v-4zm8 4h4v-4h-4v4zm8-4v4h4v-4h-4z"></path>
							</svg>
							<i>4h</i>
						</article>
					</li>
					<li>
						<article className={`${styles.logo} ${styles.nlogo2}`}></article>
						<article className={styles.content}><p><span>Your job alert</span>Sanjay Kr., <i>19</i> new jobs for <i>UI Developer</i> in <i>Noida Area</i></p></article>
						<article className={styles.settingsDays}>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" data-supported-dps="24x24" fill="currentColor" width="24" height="24" focusable="false">
								<path d="M2 10h4v4H2v-4zm8 4h4v-4h-4v4zm8-4v4h4v-4h-4z"></path>
							</svg>
							<i>4d</i>
						</article>
					</li>
				</ul>
			</Row>
		</>
	)
}