import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import styles from "./myaccount.module.scss";
export default function MyAccount(props) {
	return (
		<Container fluid={true} className={styles.myAccount}>
			<Container>
				<Row>
					<section className={styles.boxWrapper}>
						<section className={styles.box}>
							<article className={styles.titleWrapper}>
								<h2>My Account</h2>
								<span className={styles.subTitle}>
									View and edit your personal info below.
								</span>
							</article>
							<article className={styles.formCtrlWrapper}>
								<p className={styles.staticText}>
									Login Email
									<span>example@gmail.com <i></i></span>
								</p>
								<p className={styles.staticText}>
									Your Community Page URL
									<span>This is your own personalised URL that other member can see. <b>https://smita58.wixsite.com/myhclsite/profile/name</b></span>
								</p>
								<form name="myAccount">
									<article className={styles.formWrapper}>
										<article className={styles.formCtrl}>
											<input type="text" name="name" placeholder="name" />
										</article>
										<article className={styles.formCtrl}>
											<input type="text" name="firstname" placeholder="First name" />
										</article>
										<article className={styles.formCtrl}>
											<input type="text" name="lastname" placeholder="Last name" />
										</article>
										<article className={styles.formCtrl}>
											<input type="text" name="email" placeholder="Email address" />
										</article>
										<article className={styles.formCtrl}>
											<input type="tel" name="phone" placeholder="Phone" />
										</article>
									</article>
									<article className={styles.btnCtrl}>
										<button type="button" disabled> Update Career Profile</button>
									</article>
								</form>
							</article>
						</section>
					</section>
				</Row>
			</Container>
		</Container>
	)
}