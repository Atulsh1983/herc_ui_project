import React, { Component } from 'react';
import Router from 'next/router';
import ProfileImg from './profileImg';
import styles from "./leftNav.module.scss";
class LeftNav extends Component {
	constructor(props) {
		super(props);
		this.state = {}
		this.componentChangeHandler = this.componentChangeHandler.bind(this);
	}
	componentChangeHandler(componentName) {
		switch (componentName) {
			case "myprofile":
				typeof window !== 'undefined' && localStorage.setItem('componentName', componentName)
				Router.push('/myprofile', '/myprofile', { shallow: true });
				break;
		}
	}
	render() {
		let componentName = typeof window !== 'undefined' && localStorage.getItem('componentName');
		return (
			<article className={styles.leftNav}>
				<article className={styles.leftTop}>
					<article className={styles.profileImgWrapper}>
						<ProfileImg />
					</article>
					<article className={styles.tabNav}>
						<ul>
							<li className={componentName === "myprofile" ? styles.active : ""} onClick={() => this.componentChangeHandler("myprofile")}>My Profile</li>
							<li>My Connections</li>
							<li>My Messages</li>
							<li>My Events</li>
							<li>My Newsfeed</li>
							<li>My Advice Log</li>
							<li>My Library</li>
						</ul>
					</article>
				</article>
			</article>
		)
	}
}

export default LeftNav;