import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import MyProfile from "./myprofile";
import MyStatus from "./mystatus";
import MyAccount from "./myaccount";
import Notifications from "./notifications";
import LeftNav from "./leftNav";
import styles from "./dashboard.module.scss";
export default function Dashboard(props) {
	//console.log(typeof window !== 'undefined' && location.pathname.includes("/mystatus"), 'componentName')
	return (
		<Container>
			<Row>
				<section className={styles.boxWrapper}>
					<article className={styles.leftBox}>
						<LeftNav />
					</article>
					<article className={styles.rightBox}>
						{typeof window !== 'undefined' && localStorage.getItem('componentName') === 'myprofile' && <MyProfile />}
						{typeof window !== 'undefined' && localStorage.getItem('componentName') === 'signIn' && <MyStatus />}
						{typeof window !== 'undefined' && localStorage.getItem('componentName') === 'mystatus' && <MyStatus />}
						{typeof window !== 'undefined' && localStorage.getItem('componentName') === 'signUp' && <MyAccount />}
						{typeof window !== 'undefined' && localStorage.getItem('componentName') === 'notifications' && <Notifications />}
					</article>
				</section>
			</Row>
		</Container>
	)
}


