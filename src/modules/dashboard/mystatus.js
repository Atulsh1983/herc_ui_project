import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import styles from "./mystatus.module.scss";
export default function MyStatus(props) {
	return (
		<Container fluid={true} className={styles.myStatus}>
			<Container>
				<Row>
					<section className={styles.boxWrapper}>
						<section className={styles.leftBox}>
							<article className={styles.titleWrapper}>
								<h2>My Status</h2>
								<span className={styles.subTitle}>
									Post a daily status: upload an article to share
					</span>
							</article>
							<article className={styles.card}>
								<h4>Feature Content</h4>
								<p>Articles Videos: Form our HC writers, from your connections - what's new today!</p>
								<p>(Note: show only a couple and a horizontal scrollbar)</p>
								<p>note: scrollbar of articles and also allow for comments and likes...</p>
							</article>
							<article className={styles.card}>
								<h4>Content based on Interest</h4>
								<p>Articles or Videos: What's new today! based on my interest (role, specialization  and strengths and core value (Note: show only a couple and horizontal scrollbar ))</p>
							</article>
							<article className={styles.card}>
								<h4>Feature Content</h4>
								<p>Articles Videos: Form our HC writers, from your connections - what's new today!</p>
								<p>(Note: show only a couple and a horizontal scrollbar)</p>
								<p>note: scrollbar of articles and also allow for comments and likes...</p>
							</article>
							<article className={styles.card}>
								<h4>Content based on Interest</h4>
								<p>Articles or Videos: What's new today! based on my interest (role, specialization  and strengths and core value (Note: show only a couple and horizontal scrollbar ))</p>
							</article>
						</section>
						<section className={styles.rightBox}>
							<article className={`${styles.card} ${styles.justifyContent}`}>
								<h4>Events</h4>
								<p>Local relevant events</p>
								<article className={styles.imgWrapper1}></article>
							</article>
							<article className={`${styles.card} ${styles.justifyContent}`}>
								<h4>Employer Reviews</h4>
								<p>Employer Spotliht</p>
								<article className={styles.imgWrapper2}></article>
							</article>
							<article className={styles.card}>
								<h4>Advertised Content</h4>
								<p>Advertisements for jobs from employers</p>
								<article className={styles.imgWrapper}>
									<span className={styles.clogo1}></span>
									<span className={styles.clogo2}></span>
									<span className={styles.clogo3}></span>
								</article>
							</article>
						</section>
					</section>
				</Row>
			</Container>
		</Container>
	)
}