'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
//var replace = require('gulp-string-replace');
const replace = require('replace-in-file');
const options = {
    files: './src/**/*.js',
    from: /.scss/g,
    to: '.css',
};
var paths = {
    styles: {
        src: './src/**/*.scss',
        dest: './src'
    },
    scripts: {
        src: './src/**/*.js',
        dest: './src'
    }
}
function scss() {
    return gulp.src(paths.styles.src)
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({ outputStyle: 'compressed' }))
        .pipe(postcss([autoprefixer()]))
        .pipe(gulp.dest(paths.styles.dest));
}

exports.sass = scss

function rename() {
    return gulp.src(paths.scripts.src)
        .pipe(replace('.scss', '.css'))
        .pipe(gulp.dest(paths.scripts.dest));
}
gulp.task('scss', async (done) => {
    const results = await replace(options);
    done();
});
exports.rename = rename
function watch() {

    scss()

    gulp.watch(paths.styles.src, scss);
}
exports.watch = watch