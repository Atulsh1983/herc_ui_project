const next = require('next');
var compression = require('compression');
const express = require('express');

const dev = process.env.NODE_ENV !== 'production';
const PORT = process.env.PORT || 3000;
const app = next({ dir: './src', dev });
const handle = app.getRequestHandler();

app.prepare().then(() => {
    const server = express();
    server.use(express.json());
    server.use(compression());
    server.post('/api/login', (req, res) => {
        const { email, password } = req.body;
        return res.json({
            email,
            password,
            success: true
        });
    });
    server.get('*', (req, res) => {
        return handle(req, res);
    });

    server.listen(PORT, err => {
        if (err) throw err;
        console.log(`localhost is running on PORT ${PORT}`);
    });
});
